package com.example.sendevent.Controller;

import com.example.sendevent.Messaging.EventSender;
import com.example.sendevent.Model.Event;
import com.example.sendevent.Service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/event")
public class EventController {
    @Autowired
    private EventService eventService;

    @Autowired
    private EventSender eventSender;
    @PostMapping(path = "/add") // Map ONLY POST Requests
    public @ResponseBody String DeleteEvent(@RequestParam int eventId) {
        System.out.println("ID EVENT "+eventId);
        Event event = eventService.getEvent(eventId);
        System.out.println("Aya");
        boolean sent = eventSender.send(event);
        System.out.println("Aya2");
        return sent ? "Add Event To Rabbit MQ Successfully" : "Add Event To Rabbit MQ Failed";
    }



}
