package com.example.sendevent.Service;

import com.example.sendevent.Model.Event;
import com.example.sendevent.Repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EventService {

    @Autowired
    EventRepository eventRepository;

    public Event getEvent(int eventId) {
        System.out.println("Service Event Id "+eventId);
        Optional<Event> event = eventRepository.findById(eventId);
        return event.get();
    }
}
