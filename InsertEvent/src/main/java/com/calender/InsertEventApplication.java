package com.calender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsertEventApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsertEventApplication.class, args);
    }

}
