package com.example.sendevent.Repository;

import com.example.sendevent.Model.Event;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<Event,Integer>
{
}
