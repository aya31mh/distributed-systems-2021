package com.calender;

import com.calender.Model.Event;
import com.calender.Service.EventService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableBinding(Sink.class)
public class DeleteEventApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeleteEventApplication.class, args);
    }

    @Autowired
    EventService service;


    @StreamListener(Sink.INPUT)
    public void receiveEvent(Event event) throws JsonProcessingException {
        System.out.println("AAAAAAAAAAAAAAA");
        service.DeleteEvent(event);
    }
}
