package com.calender.Service;

import com.calender.Model.Event;
import com.calender.Repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventService {
    @Autowired
    private EventRepository eventRepository;

    public String DeleteEvent(final Event event) {
        System.out.println("EVENT = " + event.getName());
        eventRepository.delete(event);
        return "Delete Event Successfully";
    }
}
