package com.example.sendevent.Messaging;

import com.example.sendevent.Model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class EventSender {

    @Autowired
    private Source source;

    public boolean send(Event event) {
        return this.source.output().send(MessageBuilder.withPayload(event).build());
    }
}
